package chap1;

public class IsItFurry {
    static interface Manual{}
    static class Furry implements Manual{}
    static class Chipmunk extends Furry{}

    public void test(){
        Chipmunk chipmunk = new Chipmunk();
        Manual manual = chipmunk;
        Furry furry = chipmunk;
        int result=0;

        if (chipmunk instanceof Manual) result+=1;
        if (chipmunk instanceof Furry) result+=2;
        if (null instanceof Chipmunk) result+=4;
        System.out.println("The result is: "+ result);
    }
}
