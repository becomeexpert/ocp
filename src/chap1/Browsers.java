package chap1;

public class Browsers {

    static class Browser{
        public void go(){
            System.out.println("Inside Browser");
        }
    }
    static class FireFox extends Browser{
        public void go(){
            System.out.println("Inside Firefox");
        }
    }
    static class IE extends Browser{
        @Override
        public void go() {
            System.out.println("Inside IE");
        }
    }
    public void test(){
        Browser b = new FireFox();
        // Browser Firefox cannot be cast to Browser IE
        IE ie = (IE) b;
        ie.go();
    }
}
