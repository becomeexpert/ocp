package chap1;

import sun.font.CreatedFontTracker;

public class NestedClass {

    private String name;

    public class InnerClass implements InnerInterface{
        private int repeat = 2;


        public void go() {
            final int oldage =12;
            NestedClass.this.name = "ZOBO";
            System.out.println("Name: " + name);
            class gone{
                private int age =45;
                 private void go(){
                    System.out.println(oldage);
                 }
            }
           new gone().go();
        }

        @Override
        public String getName() {
            if (null instanceof NestedClass)
                return name;
            return name;
        }
    }
    abstract class Person{
        abstract void calculAge();
    }
    public void getAge(){
        //anonymous class
        Person person = new Person() {
            @Override
            void calculAge() {
                System.out.println("Votre age est: "+(2020-1994));
            }
        };
        person.calculAge();
    }
    private interface InnerInterface{
        String getName();
    }
    //static nested class
   final static class Nested{
        private int price =56;
        private static int age =78;
        private String getName(){
            //return NestedClass.this.name; ne marche pas car Nested class est une class static
            return new NestedClass().name;
        }
    }
    public void callInner(){
        InnerClass innerClass = new InnerClass();
        innerClass.go();
        getAge();
        Nested nested = new Nested();
        nested.price = 36;

        System.out.println("nested class price value: "+nested.price);
        String message = "bonjour";
        class test{
            private String odlName = NestedClass.this.name;
            public void test(){
                System.out.println(message);
            }
        }

    }
}
