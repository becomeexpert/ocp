package chap1;

public class VirtualMethods {

    private void feedAnimal(Animal animal){
        animal.feed();


    }

    public void testVirtualMethods(){
        feedAnimal(new Cow());
        feedAnimal(new Bird());
        feedAnimal(new Lion());
    }
}

abstract class Animal{
    public abstract void feed();
    public void test(){

    }
}
 class Cow extends Animal {
    @Override public void feed(){
        addHay();
    }
    private void addHay(){
        System.out.println("Hello! I'm a Cow.");
    }
 }
 class Bird extends Animal {
   @Override public void feed(){
        addSeed();
    }
    private void  addSeed(){
        System.out.println("Hello! I'm a Bird.");
    }
 }

 class Lion extends Animal {
   @Override public void feed(){
        addMeat();
    }
    private void addMeat(){
        System.out.println("Hello! I'm a lion.");
    }
 }