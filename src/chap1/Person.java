package chap1;

public class Person {
    private int id;
    private String name;
    private int age;

    public Person(int id, String name,int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }
    public boolean equals(Object object){
        if (!(object instanceof Person))
            return false;
        Person p = (Person) object;
        return p.name.equals(this.name);
    }
    public int hashCode(){
        return this.id;
    }

}
