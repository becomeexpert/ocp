package chap1;

public enum EnumSeason {
    WINTER("Low"){
        @Override
        public void printHours() {
            System.out.println("I am a winter");
        }
    }, SPRING("Medium"){
        @Override
        public void printHours() {
            System.out.println("I am a spring");
        }
    }, SUMMER("High"){
        @Override
        public void printHours() {
            System.out.println("I am a summer");
        }
    }, FALL("Medium");
    public void printHours(){System.out.println("default implementation.");}
    public void printHours(String overload){System.out.println("default implementation "+overload);}

    private String expectedVisitors;
    //only private by default
    EnumSeason(String expectedVisitors){
        this.expectedVisitors = expectedVisitors;
    }
    public void printExpectedVisitors(){
        System.out.println(expectedVisitors);
    }


}
