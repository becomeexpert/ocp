package main;

import chap1.*;

import java.time.LocalDateTime;

public class Main {

    enum Flavour{
        VALILLE, CHOCOLATE, AROM
    }
    public static final void main(String args[]){
        //test virtual methods
        //new VirtualMethods().testVirtualMethods();

        // test enums
        //testEnum.test();

        // test inner class
        NestedClass nestedClass = new NestedClass();
        nestedClass.callInner();

       NestedClass.InnerClass innerClass = nestedClass.new InnerClass();
        innerClass.go();
        System.out.println(Flavour.CHOCOLATE.ordinal());

        //test equals & hashCode
        Person p1 = new Person(1,"Yvon ZOBO", 25);
        Person p2 = new Person(2, "Yvon ZOBO", 26);
        System.out.println(p1.equals(p2));
        System.out.println(p1.hashCode()==p2.hashCode());

        //Inner class test
        //new Browsers().test(); throws ClassCastException

        //test instanceOf
        new IsItFurry().test();

    }
}
class testEnum{
    public static void test(){
        for (EnumSeason enumSeason:EnumSeason.values()) {
            System.out.println(enumSeason.name()+" : "+enumSeason.ordinal());
        }
        EnumSeason.SUMMER.printExpectedVisitors();
        EnumSeason.SUMMER.printHours();
        EnumSeason.FALL.printHours();
        EnumSeason.WINTER.printHours(LocalDateTime.now().toString());

    }
}
